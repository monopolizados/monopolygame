namespace MonopoliyBusiness.money {
    /// <summary>
    /// Class that represent the money of Bank
    /// </summary>
    public abstract class IMoney {

        /// <summary>
        /// Create the type of bill
        /// </summary>
        /// <param name="typeBill">int - type of bill</param>
        /// <returns>bill</returns>
        public abstract IBill makeBill(int typeBill);
    }
}