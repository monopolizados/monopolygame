namespace MonopoliyBusiness.money {
    /// <summary>
    /// This class define $10 bill
    /// </summary>
    public class Bill10 : IBill
    {
        /// <summary>
        /// Declare the type value: int and it's assigned his value
        /// </summary>
        private int value = 10;

        /// <summary>
        /// Return the value of bill
        /// </summary>
        /// <returns>int</returns>
        public int GetValue()
        {
            return this.value;
        }
    }
}