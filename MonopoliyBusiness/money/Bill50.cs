namespace MonopoliyBusiness.money {
    /// <summary>
    /// This class define the $50 bill
    /// </summary>
    public class Bill50 : IBill
    {
        /// <summary>
        /// Declare the type value: int and it's assigned his value
        /// </summary>
        private int value = 50;
        /// <summary>
        /// Return the value of bill
        /// </summary>
        /// <returns>int</returns>
        public int GetValue()
        {
            return this.value;
        }
    }
}