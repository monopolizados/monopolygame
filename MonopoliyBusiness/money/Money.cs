using System;

namespace MonopoliyBusiness.money {
    /// <summary>
    /// Define the type of bills that can be creates
    /// </summary>
    public class Money : IMoney
    {/// <summary>
    /// Define each one of the types of denomination of bills
    /// </summary>
    /// <param name="typeBill">int - type of bill</param>
    /// <returns>return the bill</returns>
        public override IBill makeBill(int typeBill)
        {
           
            IBill bill;
            if(typeBill == 500){
                bill = new Bill500();
            }else if(typeBill == 200) {
                bill = new Bill200();
            }else if(typeBill == 100) {
                bill = new Bill100();
            }else if(typeBill == 50) {
                bill = new Bill50();
            }else if(typeBill == 20) {
                bill = new Bill20();
            }else if(typeBill == 10){
                bill = new Bill10();
            }else {
                throw new Exception("Any bill type found");
            }

            return bill;
        }
    }
}