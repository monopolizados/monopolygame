namespace MonopoliyBusiness.money{
    /// <summary>
    /// Define the interface for the bills
    /// </summary>
    public interface IBill
    {
        /// <summary>
        /// Obtain value
        /// </summary>
        /// <returns></returns>
        int GetValue();
    }
}