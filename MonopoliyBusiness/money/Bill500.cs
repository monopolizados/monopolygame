namespace MonopoliyBusiness.money{
    /// <summary>
    /// This class define the $500 bill
    /// </summary>
    public class Bill500 : IBill
    {
        /// <summary>
        /// Declare the type value: int and it's assigned his value 
        /// </summary>
        private int value = 500;
        /// <summary>
        /// Return the value of bill
        /// </summary>
        /// <returns>int</returns>
        public int GetValue()
        {
            return this.value;
        }
    }
}