using System.Collections.Generic;

namespace MonopoliyBusiness.money{
    /// <summary>
    /// Defined the amount of bills for each denomination
    /// </summary>
    public class MakeBills {

        public List<IBill> GetBills(int initialMoney){
            int cantidaPorCoret = initialMoney / 6;           

            //for 500 bills
            List<IBill> totalCorte500 =new List<IBill>();
            int totalCortes500 = cantidaPorCoret/500;
            for(int i = 1; i<= totalCortes500; i++){
                totalCorte500.Add(new Money().makeBill(500));
            }

            //for 200 bills
            List<IBill> totalCorte200 =new List<IBill>();
            int totalCortes200 = cantidaPorCoret/200;
            for(int i = 1; i<= totalCortes200; i++){
                totalCorte200.Add(new Money().makeBill(200));
            }

            //for 100 bills
            List<IBill> totalCorte100 =new List<IBill>();
            int totalCortes100 = cantidaPorCoret/100;
            for(int i = 1; i<= totalCortes100; i++){
                totalCorte100.Add(new Money().makeBill(100));
            }

            //for 50 bills
            List<IBill> totalCorte50 =new List<IBill>();
            int totalCortes50 = cantidaPorCoret/50;
            for(int i = 1; i<= totalCortes50; i++){
                totalCorte50.Add(new Money().makeBill(50));
            }

            //for 20 bills
            List<IBill> totalCorte20 =new List<IBill>();
            int totalCortes20 = cantidaPorCoret/20;
            for(int i = 1; i<= totalCortes20; i++){
                totalCorte20.Add(new Money().makeBill(20));
            }

            //for 10 bills
            List<IBill> totalCorte10 =new List<IBill>();
            int totalCortes10 = cantidaPorCoret/10;
            for(int i = 1; i<= totalCortes10; i++){
                totalCorte10.Add(new Money().makeBill(10));
            }
            return null;
        }
    }
}