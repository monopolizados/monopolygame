namespace MonopoliyBusiness.money {
    /// <summary>
    /// this class define $20 bill
    /// </summary>
    public class Bill20 : IBill
    {
        /// <summary>
        /// Declare the type value: int and it's assigned his value
        /// </summary>
        private int value = 20;
        /// <summary>
        /// Return the value of bill
        /// </summary>
        /// <returns>int</returns>
        public int GetValue()
        {
            return this.value;
        }
    }
}