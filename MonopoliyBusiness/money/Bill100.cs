namespace MonopoliyBusiness.money {
    /// <summary>
    /// This class define $100 bill
    /// </summary>
    public class Bill100 : IBill
    {
        /// <summary>
        /// Declare the type value: int and it's assigned his value
        /// </summary>
        private int value = 100;
        /// <summary>
        /// Return the value of bill
        /// </summary>
        /// <returns>int</returns>
        public int GetValue()
        {
            return this.value;
        }
    }
}